# Gravity: 模拟pH计V2

DFRobot Gravity: 模拟pH计V2专门用于测量溶液的pH，衡量溶液的酸碱程度，常用于鱼菜共生、水产养殖、环境水检测等领域。

![](./python/_images/featured.png)

# 说明
Mind+的Python模式用户库中加载。

# 积木

![](./python/_images/block.png)

# 程序实例

- 示例程序-行空板

![](./python/_images/explame.png)

- 示例程序-其他主板

![](./python/_images/explame_uno.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|python|备注|
|-----|-----|-----|-----|:-----:|-----|
|uno|||||||
|micro:bit|||||||
|mpython|||||||
|arduinonano|||||||
|leonardo|||||||
|mega2560|||||||
|行空板||||√|||

# 贡献与支持

如果您对这个项目感兴趣，请Star它，如果您在使用中发现了问题，欢迎您提交问题（issue）

# 更新日志

V0.0.1 基础功能完成

V0.0.2 更新积木格式

V0.0.2 更新积木初始化
