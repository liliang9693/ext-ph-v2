//% color="#FF8C00" iconWidth=50 iconHeight=40
namespace pH_V2{

    //% block="Read Analog Pin [PIN]  pH Sensor (V2)" blockType="reporter"
    //% PIN.shadow="normal"   PIN.defl="pin1" 
    export function readPH(parameter: any, block: any) {
        let pin=parameter.PIN.code
        let obj=parameter.PIN.parType
        console.log(parameter)
        //根据输入积木生成对象名
        if(obj=="unihiker.unihikerPin"){//行空板引脚
            obj=extractPinCode(pin)
        }
       else if(obj=="data_stringmc"){//变量
          obj=pin
        }
        else if(obj=="text_normal"){//直接输入
            obj=pin
         }
         else{
            obj=pin
         }

        Generator.addImport(`from dfrobot_ph import PH2`)
        Generator.addInit(`init_ph2_${obj}`,`ph2_${obj} = PH2(${pin})`)
        Generator.addCode(`ph2_${obj}.read_ph()`)

        

    }

    function extractPinCode(input: string): string | null {
        // 正则表达式匹配 "Pin.Px" 风格的字符串
        const match = input.match(/Pin\.P(\d+)/);
        // 如果匹配成功，返回匹配的部分（不包括"Pin."）
        return match ? match[1] : null;
      }


}
